require(rgdal)

# elevation

srtm <- readGDAL(
                 "/home/steko/gisdata/srtm/norditalia.tif",
                 output.dim=c(1000,1000))

# 400 AD

biz400 <- readOGR(
                  "/home/steko/tesi/mappe/bizantini/400.kml",
                  layer="Layer #0",
                  pointDropZ=TRUE)
elev400 <- overlay(srtm, biz400)
plot(
     density(na.omit(elev400$band1[elev400$band1>-100])),
     col="red",
     xlim=c(-50,1500),
     ylim=c(0,0.005))

# 533 AD

biz533 <- readOGR(
                  "/home/steko/tesi/mappe/bizantini/533.kml",
                  layer="Layer #0",
                  pointDropZ=TRUE)
elev533 <- overlay(srtm, biz533)
lines(
     density(na.omit(elev533$band1[elev533$band1>-100])),
     col="blue")

# 612 AD

biz612 <- readOGR(
                  "/home/steko/tesi/mappe/bizantini/612.kml",
                  layer="Layer #0",
                  pointDropZ=TRUE)
elev612 <- overlay(srtm, biz612)
lines(
     density(na.omit(elev612$band1[elev612$band1>-100])),
     col="green")


# 690 AD

biz690 <- readOGR(
                  "/home/steko/tesi/mappe/bizantini/690.kml",
                  layer="Layer #0",
                  pointDropZ=TRUE)
elev690 <- overlay(srtm, biz690)
lines(
     density(na.omit(elev690$band1[elev690$band1>-100])),
     col="cyan")

# Overall elevation

lines(density(srtm$band1[srtm$band1>-100]), col="brown")
